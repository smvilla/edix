#/bin/bash

set -x

#Recopilar
mkdir data
cd data
wget https://datahub.io/core/population/r/population.csv

#Limpiar
cat population.csv | tail -n +2 | sed 's/,/\t/g' | grep 'Spain' > data_population_spain.tsv

#Visualizar
cat data_population_spain.tsv | gnuplot -e "set terminal dumb; plot '<cat' using 3:4"
