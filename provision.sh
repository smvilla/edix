#!/bin/bash

git config --global user.name "smvilla"
git config --global user.email "smvillaverde@gmail.com"

apt-get install -y emacs
apt-get install -y git
apt-get install -y wget
apt-get install -y grep
apt-get install -y genuplot
